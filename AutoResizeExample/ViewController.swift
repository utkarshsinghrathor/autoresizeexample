//
//  ViewController.swift
//  AutoResizeExample
//
//  Created by Mac-6 on 08/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var v1 : UIView = UIView() ;
    
    
    @IBOutlet var pressTheButton: UIButton!
    
    
    @IBAction func actionForPressTheButton(sender: AnyObject)
    {
        
        var scroll  : UIScrollView = UIScrollView() ;
        
        self.view.addSubview(scroll) ;
        
        scroll.backgroundColor =  UIColor.blueColor() ;
        
        NSLayoutConstraint.activateConstraints([
            scroll.topAnchor.constraintEqualToAnchor(self.view.topAnchor),
            scroll.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor) ,
            scroll.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor) ,
            scroll.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor)
            ]) ;
        
        scroll.translatesAutoresizingMaskIntoConstraints = false ;
        
        var previous :UILabel! = nil ;
        
        var constraintsArray : [NSLayoutConstraint] = [NSLayoutConstraint]() ;
        
        for( var count = 0 ; count < 1000 ; count++ )
        {
            
            
            let label : UILabel = UILabel() ;
            
            label.text = "Utkarsh "  + String(count+1) ;

            constraintsArray.append(label.trailingAnchor.constraintEqualToAnchor(scroll.trailingAnchor)) ;
            constraintsArray.append(label.leadingAnchor.constraintEqualToAnchor(scroll.leadingAnchor)) ;
            constraintsArray.append(label.heightAnchor.constraintEqualToConstant(50)) ;

            label.translatesAutoresizingMaskIntoConstraints = false ;

            
            label.textAlignment = .Center
            scroll.addSubview(label) ;
            
            if( count == 0)
            {
                print("Creating the first UILabel") ;
                
               constraintsArray.append(label.topAnchor.constraintEqualToAnchor(scroll.topAnchor)) ;
                
            }
            else if( count == 999)
            {
                print("Creating the rest of the UILabel") ;
                constraintsArray.append(label.bottomAnchor.constraintEqualToAnchor(scroll.bottomAnchor)) ;
                constraintsArray.append(label.topAnchor.constraintEqualToAnchor(previous.bottomAnchor)) ;
                
            }
            else
            {
                constraintsArray.append(label.topAnchor.constraintEqualToAnchor(previous.bottomAnchor)) ;
            }
            
            previous = label ;
            
        }
        
        print("Now applying the constraints") ;
        NSLayoutConstraint.activateConstraints(constraintsArray) ;
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

